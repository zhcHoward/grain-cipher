def f(lfsr):
    # S i+80 = S i+62 + S i+51 + S i+38 + S i+23 + S i+13 + S i
    output = (lfsr >> 17 ^ lfsr >> 28 ^ lfsr >> 41 ^
              lfsr >> 56 ^ lfsr >> 66 ^ lfsr >> 79) & 1
    return output


def g(nfsr, si):
    '''
    B i+80 = S i + B i+63 + B i+60 + B i+52 + B i+45 + B i+37 + B i+33 + B i+28 + B i+21 +
            +B i+15 + B i+9 + B i + B i+63 B i+60 + B i+37 B i+33 + B i+15 B i+9 +
            +B i+60 B i+52 B i+45 + B i+33 B i+28 B i+21 + B i+63 B i+45 B i+28 B i+9 +
            +B i+60 B i+52 B i+37 B i+33 + B i+63 B i+60 B i+21 B i+15 +
            +B i+63 B i+60 B i+52 B i+45 B i+37 + B i+33 B i+28 B i+21 B i+15 B i+9 +
            +B i+52 B i+45 B i+37 B i+33 B i+28 B i+21
    '''
    output = (si ^ nfsr >> 16 ^ nfsr >> 19 ^ nfsr >> 27 ^ nfsr >> 34 ^ nfsr >> 42 ^ nfsr >> 46 ^ nfsr >> 51 ^ nfsr >> 58 ^ nfsr >> 64 ^ nfsr >> 70 ^ nfsr >> 79 ^ (nfsr >> 16 & nfsr >> 19) ^ (nfsr >> 42 & nfsr >> 46) ^ (nfsr >> 64 & nfsr >> 70) ^ (nfsr >> 19 & nfsr >> 27 & nfsr >> 34) ^ (nfsr >> 46 & nfsr >> 51 & nfsr >> 58) ^ (nfsr >> 16 & nfsr >> 34 &
                                                                                                                                                                                                                                                                                                                                         nfsr >> 51 & nfsr >> 70) ^ (nfsr >> 19 & nfsr >> 27 & nfsr >> 42 & nfsr >> 46) ^ (nfsr >> 16 & nfsr >> 19 & nfsr >> 58 & nfsr >> 64) ^ (nfsr >> 16 & nfsr >> 19 & nfsr >> 27 & nfsr >> 34 & nfsr >> 42) ^ (nfsr >> 46 & nfsr >> 51 & nfsr >> 58 & nfsr >> 64 & nfsr >> 70) ^ (nfsr >> 27 & nfsr >> 34 & nfsr >> 42 & nfsr >> 46 & nfsr >> 51 & nfsr >> 58)) & 1
    return output


def h(lfsr, nfsr):
    '''
    h(x) = x1 + x4 + x0x3 + x2x3 + x3x4 +x0x1x2 + x0x2x3 + x0x2x4 + x1x2x4 + x2x3x4
    x0 = S i+3
    x1 = S i+25
    x2 = S i+46
    x3 = S i+64
    x4 = B i+63
    '''
    x0 = lfsr >> 76
    x1 = lfsr >> 54
    x2 = lfsr >> 33
    x3 = lfsr >> 15
    x4 = nfsr >> 16
    output = (x1 ^ x4 ^ (x0 & x3) ^ (x2 & x3) ^ (x3 & x4) ^ (
        x0 & x1 & x2) ^ (x0 & x2 & x4) ^ (x1 & x2 & x4) ^ (x2 & x3 & x4)) & 1
    return output

'''
main encryption process
'''


def grain(key, iv):
    '''
    Load key and IV into NFSR and LFSR seperately
    '''

    nfsr = key
    lfsr = iv << 16 | 0xffff
    '''
    Key Initialization
    '''
    for i in range(160):
        lfsr_update_bit = f(lfsr)
        nfsr_update_bit = g(nfsr, lfsr >> 79)
        h_output_bit = h(lfsr, nfsr)
        grain_output_bit = h_output_bit ^ (
            nfsr >> 78 ^ nfsr >> 77 ^ nfsr >> 75 ^ nfsr >> 69 ^ nfsr >> 48 ^ nfsr >> 36 ^ nfsr >> 23) & 1
        new_lfsr_bit = lfsr_update_bit ^ grain_output_bit
        new_nfsr_bit = nfsr_update_bit ^ grain_output_bit
        lfsr = (lfsr << 1 | new_lfsr_bit) & 0xffffffffffffffffffff
        nfsr = (nfsr << 1 | new_nfsr_bit) & 0xffffffffffffffffffff

    '''
    Generating key stream
    '''
    keystream = 0
    keystream2 = 0
    for i in range(80):
        # print(i)
        # strlfsr = hex(lfsr)
        # strnfsr = hex(nfsr)
        # print('lfsr = ', strlfsr[2:6], strlfsr[6:10],
        #       strlfsr[10:14], strlfsr[14:18], strlfsr[18:])
        # print('nfsr = ', strnfsr[2:6], strnfsr[6:10],
        #       strnfsr[10:14], strnfsr[14:18], strnfsr[18:])
        lfsr_update_bit = f(lfsr)
        nfsr_update_bit = g(nfsr, lfsr >> 79)
        # print('lfsr_update_bit = ', lfsr_update_bit)
        # print('nfsr_update_bit = ', nfsr_update_bit)
        h_output_bit = h(lfsr, nfsr)
        # print('h(x) = ', h_output_bit)
        grain_output_bit = h_output_bit ^ (
            nfsr >> 78 ^ nfsr >> 77 ^ nfsr >> 75 ^ nfsr >> 69 ^ nfsr >> 48 ^ nfsr >> 36 ^ nfsr >> 23) & 1
        # print('grain output = ', grain_output_bit)
        keystream = keystream << 1 | grain_output_bit
        keystream2 = keystream2 | (grain_output_bit << i)
        # print(grain_output_bit)
        lfsr = (lfsr << 1 | lfsr_update_bit) & 0xffffffffffffffffffff
        nfsr = (nfsr << 1 | nfsr_update_bit) & 0xffffffffffffffffffff
        # print('****************')

    print('keystream = {0}'.format(hex(keystream)))
    print('keystream = {0}'.format(hex(keystream2)))
'''
unit test
'''


def main():
    key = 0
    iv = 0
    grain(key, iv)

if __name__ == '__main__':
    main()
