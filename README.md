# README #

Implementation of Grain cipher written both in C++ and Python. 

When I test those implementations with test vectors in [paper](http://www.ecrypt.eu.org/stream/p2ciphers/grain/Grain_p2.pdf), my two implementation can get the same result, but they never get the result displayed in the paper. I still try to find if there are any bugs in my implementation.


### Testing ###

* For Python, my Python version 3.4.0
  To test Python code: `python3 grain.py`
* For C++, I am using GCC 4.8.2
  To test C++ code: `g++ -std=c++11 -Wall *.cpp -I ../header -o /file path/cube_attack.out`