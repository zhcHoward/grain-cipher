#include <iostream>
using namespace std;

bool f(uint16_t lfsr[])
{
	//S i+80 = S i+62 + S i+51 + S i+38 + S i+23 + S i+13 + S i
	bool output = ((lfsr[3] >> 1) ^ lfsr[3] >> 12 ^ lfsr[2] >> 9 ^ lfsr[1] >> 8 ^ lfsr[0] >> 2 ^ lfsr[0] >> 15) & 1;
	return output;
}

bool g(uint16_t nfsr[], bool si)
{
	//B i+80 = S i + B i+63 + B i+60 + B i+52 + B i+45 + B i+37 + B i+33 + B i+28 + B i+21 +
			// +B i+15 + B i+9 + B i + B i+63 B i+60 + B i+37 B i+33 + B i+15 B i+9 +
			// +B i+60 B i+52 B i+45 + B i+33 B i+28 B i+21 + B i+63 B i+45 B i+28 B i+9 +
			// +B i+60 B i+52 B i+37 B i+33 + B i+63 B i+60 B i+21 B i+15 +
			// +B i+63 B i+60 B i+52 B i+45 B i+37 + B i+33 B i+28 B i+21 B i+15 B i+9 +
			// +B i+52 B i+45 B i+37 B i+33 B i+28 B i+21
	bool output = (si ^ nfsr[3] ^ nfsr[3] >> 3 ^ nfsr[3] >> 11 ^ nfsr[2] >> 2 ^ nfsr[2] >> 10 ^ nfsr[2] >> 14 ^ nfsr[1] >> 3 ^ nfsr[1] >> 10 ^ nfsr[0] ^ nfsr[0] >> 6 ^ nfsr[0] >> 15 ^ (nfsr[3] & nfsr[3] >> 3) ^ (nfsr[2] >> 10 & nfsr[2] >> 14) ^ (nfsr[0] & nfsr[0] >> 6) ^ (nfsr[3] >> 3 & nfsr[3] >> 11 & nfsr[2] >> 2) ^ (nfsr[2] >> 14 & nfsr[1] >> 3 & nfsr[1] >> 10) ^ (nfsr[3] & nfsr[2] >> 2 & nfsr[1] >> 3 & nfsr[0] >> 6) ^ (nfsr[3] >> 3 & nfsr[3] >> 11 & nfsr[2] >> 10 & nfsr[2] >> 14) ^ (nfsr[3] & nfsr[3] >> 3 & nfsr[1] >> 10 & nfsr[0]) ^ (nfsr[3] & nfsr[3] >> 3 & nfsr[3] >> 11 & nfsr[2] >> 2 & nfsr[2] >> 10) ^ (nfsr[2] >> 14 & nfsr[1] >> 3 & nfsr[1] >> 10 & nfsr[0] & nfsr[0] >> 6) ^ (nfsr[3] >> 11 & nfsr[2] >> 2 & nfsr[2] >> 10 & nfsr[2] >> 14 & nfsr[1] >> 3 & nfsr[1] >> 10)) & 1;
	return output;
}

bool h(bool x0, bool x1, bool x2, bool x3, bool x4)
{
	/*
	 * h(x) = x1 + x4 + x0x3 + x2x3 + x3x4 +x0x1x2 + x0x2x3 + x0x2x4 + x1x2x4 + x2x3x4
	 * x1 = S i+3
	 * x2 = S i+25
	 * x3 = S i+46
	 * x4 = S i+64
	 * x5 = B i+63
	 */
	 bool output = (x1 ^ x4 ^ (x0 & x3) ^ (x2 & x3) ^ (x3 & x4) ^ (x0 & x1 & x2) ^ (x0 & x2 & x4) ^ (x1 & x2 & x4) ^ (x2 & x3 & x4)) & 1;
	 return output;
}

void shift(uint16_t fsr[], bool new_bit)
{
	bool shift_bit;
	for (int i = 4; i > -1; --i)
	{
		shift_bit = fsr[i] >> 15 & 1;
		fsr[i] = ((fsr[i] << 1) | new_bit) & 0xffff;
		new_bit = shift_bit;
	}
	// printf("%04x%04x%04x%04x%04x\n", fsr[0],fsr[1],fsr[2],fsr[3],fsr[4]);
	// return fsr;
}

/*
 * main encryption process
 */
bool grain(uint16_t key[], uint64_t iv)
{
	/*
	 * Load key and IV into NFSR and LFSR seperately
	 */
	uint16_t  lfsr[5], nfsr[5];
	for(int i = 0; i < 5; i++)
		nfsr[i] = key[i];
	for(int i = 0; i < 4; i++)
	{
		lfsr[i] = iv >> ((3 - i) * 16) & 0xffff;
	}
	lfsr[4] = 0xffff;

	/*
	 * Key Initialization
	 */
	 bool lfsr_update_bit, nfsr_update_bit, h_output_bit, grain_output_bit;
	 bool new_lfsr_bit, new_nfsr_bit, lfsr_shift_bit, nfsr_shift_bit;

	for(int i=0; i<160; i++)
	{
		lfsr_update_bit = f(lfsr);
		nfsr_update_bit = g(nfsr, lfsr[0] >> 15 & 1);
		h_output_bit = h(lfsr[0] >> 12 & 1, lfsr[1] >> 6 & 1, lfsr[2] >> 1 & 1, lfsr[4] >> 15, nfsr[3] & 1);
		grain_output_bit = h_output_bit ^ (nfsr[0] >> 14 ^ nfsr[0] >> 13 ^ nfsr[0] >> 11 ^ nfsr[0] >> 5 ^ nfsr[1] ^ nfsr[2] >> 4 ^ nfsr[3] >> 7) & 1;
		new_lfsr_bit = lfsr_update_bit ^ grain_output_bit;
		new_nfsr_bit = nfsr_update_bit ^ grain_output_bit;
		shift(lfsr, new_lfsr_bit);
		shift(nfsr, new_nfsr_bit);
	}
	
	/*
	 * Generating key stream
	 */
	uint16_t h16_keystream = 0;
	uint64_t l64_keystream = 0;
	for(int i=0; i<80; i++)
	{
		// cout<<i<<endl;
		// printf("lfsr = %04x %04x %04x %04x %04x\n", lfsr[0],lfsr[1],lfsr[2],lfsr[3],lfsr[4]);
		// printf("nfsr = %04x %04x %04x %04x %04x\n", nfsr[0],nfsr[1],nfsr[2],nfsr[3],nfsr[4]);
		lfsr_update_bit = f(lfsr);
		nfsr_update_bit = g(nfsr, lfsr[0] >> 15 & 1);
		// cout<<"lfsr_update_bit = "<<lfsr_update_bit<<endl;
		// cout<<"nfsr_update_bit = "<<nfsr_update_bit<<endl;
		h_output_bit = h(lfsr[0] >> 12 & 1, lfsr[1] >> 6 & 1, lfsr[2] >> 1 & 1, lfsr[4] >> 15, nfsr[3] & 1);
		// cout<<"h(x) = "<<h_output_bit<<endl;
		grain_output_bit = h_output_bit ^ (nfsr[0] >> 14 ^ nfsr[0] >> 13 ^ nfsr[0] >> 11 ^ nfsr[0] >> 5 ^ nfsr[1] ^ nfsr[2] >> 4 ^ nfsr[3] >> 7) & 1;
		// cout<<"grain output = "<<grain_output_bit<<endl;
		if(i<16)
		{
			h16_keystream += grain_output_bit << (15 - i);
		}
		else
		{
			l64_keystream += (uint64_t)grain_output_bit << (79 - i);
		}
		shift(lfsr, lfsr_update_bit);
		shift(nfsr, nfsr_update_bit);
		// cout<<"*************************"<<endl;
	}
	printf("\n%04x%016lx\n", h16_keystream, l64_keystream);
	return (h16_keystream>>15);
	// return 0;
}

/*
 * unit test
 */
int main(int argc, char const *argv[])
{
	uint16_t key[5] = {0x0000,0x0000,0x0000,0x0000,0x0000};
	uint64_t IV = 0x0000000000000000;
	bool a = grain(key, IV);
	return 0;
}